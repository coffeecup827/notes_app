package com.altair.notes_app.domain.entities.read_only;

import com.altair.notes_app.base.entity.IQuery;
import com.altair.notes_app.domain.entities.interfaces.queries.IUserQueries;
import com.altair.notes_app.base.transaction.IEntity;

public interface IUser extends IEntity, IUserQueries, IQuery {

    String getFirstName();

    String getLastName();

    IUserDetail getUserDetail();

}
