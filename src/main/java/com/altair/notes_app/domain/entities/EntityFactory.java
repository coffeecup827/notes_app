package com.altair.notes_app.domain.entities;

import com.altair.notes_app.base.entity.IFactory;
import com.altair.notes_app.base.transaction.IEntity;

import java.lang.reflect.Constructor;

@SuppressWarnings("unchecked")
public class EntityFactory<TEntity> implements IFactory {

    private TEntity CreateUser(EUser user){
        EUser m = fillModel(user,EUser.class);
        m.setDetail(CreateUserDetail(m));
        User _user = new User(m);
        return  (TEntity)_user;
    }

    private EUserDetail CreateUserDetail(EUser parent){
        EUserDetail m = fillModel(parent.getDetail(),EUserDetail.class);
        m.setUser(parent);
        return m;
    }

    private UserDetail CreateUserDetail(EUserDetail userDetail){
        EUserDetail m = fillModel(userDetail,EUserDetail.class);
        UserDetail _detail = new UserDetail(m);
        return _detail;
    }

    private <C extends IEntity> C fillModel(C model,Class<C> modelClass){
        if (model == null)
        {
            try {
                Constructor<C> ctor = modelClass.getConstructor();
                model = ctor.newInstance();
            }
            catch (Exception e){
                throw new IllegalStateException("Failed To Create Model");
            }
        }
        return model;
    }

    @Override
    public  Object Create(String entityName, Object entity) {
        switch (entityName){
            case User.ModelName :
                return CreateUser((EUser) entity);
            case UserDetail.ModelName:
                return CreateUserDetail((EUserDetail) entity);
            default:
                throw new IllegalArgumentException("Entity Not Found");
        }
    }
}
