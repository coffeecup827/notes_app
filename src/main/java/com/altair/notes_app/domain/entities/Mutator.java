package com.altair.notes_app.domain.entities;

abstract class UserMutator extends UserAccessor {

    private EUser _user;

    UserMutator(EUser user){
        super(user);
        this._user = user;
    }

    //getters
    public UserDetail getUserDetail() {
        return new UserDetail(_user.getDetail());
    }

    //setters
    public void setFirstName(String s) {
        _user.setFirstName(s);
    }

    public void setLastName(String s) {
        _user.setLastName(s);
    }

}

abstract class UserDetailMutator extends UserDetailAccessor {

    private EUserDetail _userDetail;

    UserDetailMutator(EUserDetail userDetail){
        super(userDetail);
        this._userDetail = userDetail;
    }

    //getters

    public User getUser(){
        return new User(_userDetail.getUser());
    }

    //setters

    public void setEmail(String s) {
        _userDetail.setEmail(s);
    }

    public void setPhone(String s) {
        _userDetail.setPhone(s);
    }

}