package com.altair.notes_app.domain.entities;

import com.altair.notes_app.base.entity.AbstractEntity;
import com.altair.notes_app.domain.entities.read_only.IUser;
import com.altair.notes_app.domain.entities.read_only.IUserDetail;

abstract class UserAccessor extends AbstractEntity {

    private EUser _user;

    UserAccessor(EUser user){
        super(user);
        this._user = user;
    }

    public String getFirstName() {
        return _user.getFirstName();
    }

    public String getLastName() {
        return _user.getLastName();
    }

    public IUserDetail getUserDetail() {
        return (IUserDetail)new UserDetail(_user.getDetail());
    }

}

abstract class UserDetailAccessor extends AbstractEntity {

    private EUserDetail _userDetail;

    UserDetailAccessor(EUserDetail userDetail){
        super(userDetail);
        this._userDetail = userDetail;
    }

    public String getEmail() {
        return _userDetail.getEmail();
    }

    public String getPhone() {
        return _userDetail.getPhone();
    }

    public IUser getUser() {
        return (IUser) new User(_userDetail.getUser());
    }

}