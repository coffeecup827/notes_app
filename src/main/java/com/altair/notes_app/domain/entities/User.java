package com.altair.notes_app.domain.entities;

import com.altair.notes_app.domain.entities.interfaces.actions.IUserActions;
import com.altair.notes_app.domain.entities.read_only.IUser;

public class User extends UserMutator implements IUserActions, IUser {

    public static final String ModelName = EUser.modelName;

    User(EUser user){
        super(user);
    }

}
