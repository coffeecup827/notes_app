package com.altair.notes_app.domain.entities.read_only;

import com.altair.notes_app.base.entity.IQuery;
import com.altair.notes_app.domain.entities.interfaces.queries.IUserDetailQueries;
import com.altair.notes_app.base.transaction.IEntity;

public interface IUserDetail extends IEntity, IUserDetailQueries, IQuery {

    String getEmail();

    String getPhone();

    IUser getUser();

}
