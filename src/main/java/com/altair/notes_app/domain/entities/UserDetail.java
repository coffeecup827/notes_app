package com.altair.notes_app.domain.entities;

import com.altair.notes_app.domain.entities.interfaces.actions.IUserDetailActions;
import com.altair.notes_app.domain.entities.read_only.IUserDetail;

public class UserDetail extends UserDetailMutator implements IUserDetailActions, IUserDetail {

    public static final String ModelName = EUserDetail.modelName;

    UserDetail(EUserDetail userDetail){
        super(userDetail);
    }

}
