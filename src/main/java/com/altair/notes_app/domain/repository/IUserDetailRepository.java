package com.altair.notes_app.domain.repository;

import com.altair.notes_app.base.repository.IGenericRepository;
import com.altair.notes_app.domain.entities.read_only.IUserDetail;
import com.altair.notes_app.domain.entities.EUserDetail;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserDetailRepository extends IGenericRepository<EUserDetail, IUserDetail,Long> {
}
