package com.altair.notes_app.domain.repository;


import com.altair.notes_app.base.repository.IGenericRepository;
import com.altair.notes_app.domain.entities.read_only.IUser;
import com.altair.notes_app.domain.entities.EUser;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends IGenericRepository<EUser, IUser,Long> {

}
