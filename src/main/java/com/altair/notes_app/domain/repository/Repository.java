package com.altair.notes_app.domain.repository;

import com.altair.notes_app.domain.entities.User;
import com.altair.notes_app.base.transaction.IEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(scopeName = "singleton")
@SuppressWarnings("unchecked")
public class Repository {

    @Autowired
    public IUserRepository Users;

    @Autowired
    public IUserDetailRepository UserDetails;

    public <TModel extends IEntity> TModel getModelById(String modelName, Long id){
        switch (modelName){
            case User.ModelName:
                return (TModel) Users.findById(id).get();
            default:
                throw new IllegalArgumentException("Invalid Model");
        }
    }

    public <TModel extends IEntity> List<TModel> getAllModelByIds(String modelName, List<Long> ids){
        switch (modelName){
            case User.ModelName:
                return (List<TModel>) Users.findAllById(ids);
            default:
                throw new IllegalArgumentException("Invalid Model");
        }
    }

}
