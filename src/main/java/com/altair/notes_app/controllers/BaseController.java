package com.altair.notes_app.controllers;

import com.altair.notes_app.domain.repository.Repository;
import com.altair.notes_app.base.transaction.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
abstract class BaseController {

    @Autowired
    Repository Repository;

    @Autowired
    TransactionService TransactionService;

}
