package com.altair.notes_app.controllers;

import com.altair.notes_app.domain.entities.User;
import com.altair.notes_app.domain.entities.read_only.IUser;
import com.altair.notes_app.domain.entities.read_only.IUserDetail;
import com.altair.notes_app.base.transaction.TransactionUtil;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController extends BaseController {

    @GetMapping("")
    @Transactional(rollbackFor = Throwable.class)
    public long GetUsers(){

        TransactionUtil txn = TransactionService.Create(User.ModelName);

        User u;
        u = (User)txn.EntityAs();

        u.setLastName("aaa");

        u.setFirstName("bbb");

        u.getUserDetail().setEmail(Instant.now().toString());

        txn.save();

        txn.commit();

        List<IUserDetail> details = Repository.UserDetails.FindAll();

        IUserDetail a = details.get(1);

        IUser uu = a.getUser();

        IUserDetail ed = Repository.UserDetails.GetQueryInstance(Repository.UserDetails.findAll().get(1));

        IUser user = ed.getUser();

        return Repository.Users.findAll().size();
    }

}
