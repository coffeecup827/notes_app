package com.altair.notes_app.base.repository;

import com.altair.notes_app.domain.entities.EntityFactory;
import com.altair.notes_app.base.entity.IQuery;
import com.altair.notes_app.base.transaction.IEntity;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoRepositoryBean
@SuppressWarnings("unchecked")
public interface IGenericRepository<TModel extends IEntity, TQuery extends IQuery, ID extends Serializable> extends IReadOnlyRepository<TModel, ID> {

    EntityFactory _factory = new EntityFactory();;

    default List<TQuery> FindAll(){

        List<TModel> entities = findAll();

        List<TQuery> queries = new ArrayList<>();

        for (TModel entity: entities) {

            queries.add((TQuery)_factory.Create(entity.getModelName(),entity));

        }

        return queries;
    }

    default TQuery GetQueryInstance(TModel model){
        return (TQuery)_factory.Create(model.getModelName(),model);
    }

}
