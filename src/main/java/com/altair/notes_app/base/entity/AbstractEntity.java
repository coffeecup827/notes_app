package com.altair.notes_app.base.entity;

import com.altair.notes_app.base.transaction.IEntity;
import com.altair.notes_app.base.transaction.AbstractTransaction;

import java.util.Date;

@SuppressWarnings("unchecked")
public abstract class AbstractEntity<TEntity extends IEntity> extends AbstractTransaction implements IEntity {

    private TEntity _entity;

    public AbstractEntity(TEntity entity){
        super(entity);
        _entity = entity;
    }

    @Override
    public long getId() {
        return _entity.getId();
    }

    @Override
    public Date getCreatedAt() {
        return _entity.getCreatedAt();
    }

    @Override
    public Date getUpdatedAt() {
        return _entity.getUpdatedAt();
    }

    @Override
    public String getModelName() {
        return _entity.getModelName();
    }
}
