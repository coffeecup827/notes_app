package com.altair.notes_app.base.entity;

public interface IFactory <TEntity,TParam> {

    TEntity Create(String entity,TParam param);

}
