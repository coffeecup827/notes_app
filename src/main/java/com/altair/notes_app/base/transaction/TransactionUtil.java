package com.altair.notes_app.base.transaction;

import org.springframework.transaction.PlatformTransactionManager;

@SuppressWarnings("unchecked")
public class TransactionUtil<TEntity extends AbstractTransaction> extends BaseTransactionUtil {

    private TEntity _entity;

    TransactionUtil(PlatformTransactionManager transactionManager, TEntity entity, _Repository repository){
        super(transactionManager,repository);
        _entity = entity;
    }

    public TEntity EntityAs(){
        return _entity;
    }

    public void save(){
        saveEntity();
    }

    public void commit(){
        saveEntity();
        _transactionManager.commit(_txnStatus);
    }

    public void rollback(){
        _transactionManager.rollback(_txnStatus);
    }

    public boolean isCompleted(){
        return _txnStatus.isCompleted();
    }

    private void saveEntity() {
        _repository.save(_entity.getModelName(),_entity.getEntity());
    }

}
