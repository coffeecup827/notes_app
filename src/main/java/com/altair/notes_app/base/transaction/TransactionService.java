package com.altair.notes_app.base.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

@Service
@SuppressWarnings("unchecked")
public class TransactionService extends BaseTransactionService {

    @Autowired
    private PlatformTransactionManager transactionManager;

    public <TEntity extends AbstractTransaction> TransactionUtil Create(String modelName){
        TEntity entity = (TEntity) _factory.Create(modelName,null);
        TransactionUtil txn = new TransactionUtil(transactionManager, entity, _wrepository);
        return txn;
    }

//    public <TEntity extends AbstractTransaction,TModel extends IEntity> TransactionUtil Edit(TModel model){
//        TEntity entity = (TEntity) _factory.Create(model.getModelName(),model);
//        TransactionUtil txn = new TransactionUtil(transactionManager, entity, _wrepository);
//        return txn;
//    }

    public <TEntity extends AbstractTransaction,TModel extends IEntity> TransactionUtil Edit(String modelName,Long id){
        TModel model = Repository.getModelById(modelName,id);
        TEntity entity = (TEntity) _factory.Create(modelName,model);
        TransactionUtil txn = new TransactionUtil(transactionManager, entity, _wrepository);
        return txn;
    }

}
