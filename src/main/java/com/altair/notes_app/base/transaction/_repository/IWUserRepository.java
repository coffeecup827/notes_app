package com.altair.notes_app.base.transaction._repository;

import com.altair.notes_app.domain.entities.EUser;
import org.springframework.stereotype.Repository;

@Repository
public interface IWUserRepository extends IWriteOnlyRepository<EUser,Long> {

}
