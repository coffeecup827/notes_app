package com.altair.notes_app.base.transaction;

public abstract class AbstractTransaction<TEntity extends IEntity>  {

    private TEntity _entity;

    public AbstractTransaction(TEntity entity){
        _entity = entity;
    }

    TEntity getEntity(){
        return  _entity;
    }

    String getModelName() {
        return _entity.getModelName();
    }

}
