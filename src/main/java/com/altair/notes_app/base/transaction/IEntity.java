package com.altair.notes_app.base.transaction;

import java.util.Date;

public interface IEntity {

    long getId();

    Date getCreatedAt();

    Date getUpdatedAt();

    String getModelName();

}
