package com.altair.notes_app.base.transaction;

import org.springframework.transaction.*;
import org.springframework.transaction.support.DefaultTransactionDefinition;

abstract class BaseTransactionUtil {

    _Repository _repository;

    TransactionStatus _txnStatus;

    PlatformTransactionManager _transactionManager;

    BaseTransactionUtil(PlatformTransactionManager transactionManager, _Repository repository){
        _repository = repository;
        _transactionManager = transactionManager;
        TransactionDefinition _txnDef = getTxnDef();
        _txnStatus = _transactionManager.getTransaction(_txnDef);
    }

    private TransactionDefinition getTxnDef(){
        DefaultTransactionDefinition txnDef = new DefaultTransactionDefinition();
        txnDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        txnDef.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        txnDef.setTimeout(30);
        return txnDef;
    }

}
