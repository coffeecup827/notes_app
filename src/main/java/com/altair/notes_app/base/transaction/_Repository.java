package com.altair.notes_app.base.transaction;

import com.altair.notes_app.domain.entities.User;
import com.altair.notes_app.domain.entities.EUser;
import com.altair.notes_app.base.transaction._repository.IWUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "singleton")
class _Repository {

    @Autowired
    private IWUserRepository Users;

    <TEntity extends IEntity> void save(String modelName, TEntity entity){
        switch (modelName){
            case User.ModelName:
                Users.save((EUser) entity);
                break;
            default:
                throw new IllegalArgumentException("Invalid Model");
        }
    }

}
