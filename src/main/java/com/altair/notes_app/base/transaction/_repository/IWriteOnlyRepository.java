package com.altair.notes_app.base.transaction._repository;

import org.springframework.data.repository.Repository;
import org.springframework.data.repository.NoRepositoryBean;
import java.io.Serializable;

@NoRepositoryBean
interface IWriteOnlyRepository<T, ID extends Serializable> extends Repository<T, ID> {
    <S extends T> S save(S var1);
}
