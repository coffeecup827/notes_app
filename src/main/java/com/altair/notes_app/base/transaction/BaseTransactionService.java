package com.altair.notes_app.base.transaction;

import com.altair.notes_app.domain.entities.EntityFactory;
import com.altair.notes_app.base.entity.IFactory;
import com.altair.notes_app.domain.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
abstract class BaseTransactionService {

    IFactory _factory = new EntityFactory();

    @Autowired
    Repository Repository;

    @Autowired
    _Repository _wrepository;


}
